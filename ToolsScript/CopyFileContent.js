/**
 * @file
 * @author LinnYoung (946692837@qq.com)
 * @date 2020年6月15日
 * @description 讲一个文件夹下的所有脚本内容复制到一个txt中，创作初衷，申请软著需要提交代码上去
 */

const fs = require('fs');

let content = '';

function copyFileContent(src) {
    const paths = fs.readdirSync(src);

    paths.forEach((item) => {
        let _newSrc = src + '/' + item;
        const stat = fs.statSync(_newSrc);
        if (stat) {
            if (stat.isDirectory()) {
                // 是文件夹——>递归
                copyFileContent(_newSrc);
            } else if (stat.isFile()) {
                const names = item.split('.');
                if (names[1] === 'ts' && !names[2]) {
                    // copy 该格式文件的内容到一个文件
                    content += fs.readFileSync(_newSrc, 'utf-8');
                }
            }
        }
    });
}

copyFileContent('E:\\Space2\\Cat\\assets');
fs.writeFileSync('./a.txt', content);
