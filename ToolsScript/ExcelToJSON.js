/**
 * @file
 * @author LinnYoung (946692837@qq.com)
 * @date 2023年12月15日
 * @description 通过Excel表配置数值，导出为JSON文件，第一行的第用（#号）表示键值表
 */

const XLSX = require('xlsx');
const fs = require('fs');
const path = require('path');

//读取命令中的参数
let src = process.argv[2];

//读取路径文件
const files = fs.readdirSync(src);

for (let x = 0; x < files.length; ++x) {
    let fileName = files[x];
    let subName = fileName.split('.');
    if (subName[subName.length - 1] == 'xlsx') {
        //读取Excel表格数据
        xlsxFile = path.join(src, fileName);
        const workbook = XLSX.readFile(xlsxFile);
        //一个xlsx表里分表名称
        const sheet_name_list = workbook.SheetNames;
        for (let i = 0; i < sheet_name_list.length; ++i) {
            const data = XLSX.utils.sheet_to_json(
                workbook.Sheets[sheet_name_list[i]]
            );
            let typeOfKey = {};
            let jsonFile = {};
            jsonFile['desc'] = {};
            let isArr = true;
            for (let j = 0; j < data.length; ++j) {
                if (j === 0) {
                    //第一条
                    for (let key in data[j]) {
                        jsonFile['desc'][data[j][key]] = key;
                        //判断是否是key值表还是数组表
                        if (key[0] == '#') isArr = false;
                    }
                    if (isArr) {
                        jsonFile['items'] = new Array();
                    } else {
                        jsonFile['items'] = {};
                    }
                } else if (j === 1) {
                    // 存储类型
                    for (let key in data[j]) {
                        typeOfKey[key] = data[j][key];
                    }
                } else {
                    if (isArr) {
                        // 数组
                        let obj = {};
                        for (let key in data[j]) {
                            obj[data[0][key]] = data[j][key];
                        }
                        jsonFile['items'].push(obj);
                    } else {
                        let obj = {};
                        let first;
                        for (let key in data[j]) {
                            let value = data[j][key];
                            if (typeOfKey[key] == 'bool') {
                                value = value == 1 ? true : false;
                            }
                            obj[data[0][key]] = value;
                            if (!first) {
                                first = data[j][key] + '';
                            }
                        }
                        jsonFile['items'][first] = obj;
                    }
                }
            }
            
            if (!fs.existsSync('./Config')) {
              fs.mkdir('./Config', () => {
                  fs.writeFile(
                      `./Config/${sheet_name_list[i]}.json`,
                      JSON.stringify(jsonFile),
                      (err) => {
                          if (err) throw err;
                          console.log(`${sheet_name_list[i]}.json`);
                      }
                  );
              });
          } else {
              fs.writeFile(
                  `./Config/${sheet_name_list[i]}.json`,
                  JSON.stringify(jsonFile),
                  (err) => {
                      if (err) throw err;
                      console.log(`${sheet_name_list[i]}.json`);
                  }
              );
          }
        }
    }
}
