var fs = require('fs');
var path = require('path');
var crypto = require('crypto');

var manifest = {
    packageUrl: 'http://10.255.0.229:8000/remote-assets/',
    remoteManifestUrl:
        'http://10.255.0.229:8000/remote-assets/project.manifest',
    remoteVersionUrl: 'http://10.255.0.229:8000/remote-assets/version.manifest',
    version: '1.0.0',
    assets: {},
    searchPaths: [],
};

var dest = 'build/jsb-link/';
var src = 'build/jsb-link/';

// Parse arguments
var i = 2;
while (i < process.argv.length) {
    var arg = process.argv[i];

    switch (arg) {
        case '--url':
        case '-u':
            var url = process.argv[i + 1];
            manifest.packageUrl = url;
            manifest.remoteManifestUrl = url + 'project.manifest';
            manifest.remoteVersionUrl = url + 'version.manifest';
            i += 2;
            break;
        case '--version':
        case '-v':
            manifest.version = process.argv[i + 1];
            i += 2;
            break;
        case '--src':
        case '-s':
            src = process.argv[i + 1];
            i += 2;
            break;
        case '--dest':
        case '-d':
            dest = process.argv[i + 1];
            i += 2;
            break;
        default:
            i++;
            break;
    }
}
if (process.argv.length === 2) {
    const isExist = fs.existsSync(dest + 'version.manifest');
    if (!isExist) {
        manifest.version = '1.0.0';
    } else {
        const data = fs.readFileSync(dest + 'version.manifest');
        if (!data) {
            console.log('加载文件错误：' + err);
            manifest.version = '1.0.0';
        } else {
            manifest.version = '';
            const vManifest = JSON.parse(data);
            const vals = vManifest.version.split('.');
            console.log(vManifest.version);
            for (let i = 0; i < vals.length; ++i) {
                let v = vals[i];
                if (i === vals.length - 1) {
                    v = (parseInt(v) + 1).toString();
                }

                manifest.version += v;
                if (i !== vals.length - 1) {
                    manifest.version += '.';
                }
            }
            console.log(manifest.version);
        }
    }
}

function readDir(dir, obj) {
    var stat = fs.statSync(dir);
    if (!stat.isDirectory()) {
        return;
    }
    var subpaths = fs.readdirSync(dir),
        subpath,
        size,
        md5,
        compressed,
        relative;
    for (var i = 0; i < subpaths.length; ++i) {
        if (subpaths[i][0] === '.') {
            continue;
        }
        subpath = path.join(dir, subpaths[i]);
        stat = fs.statSync(subpath);
        if (stat.isDirectory()) {
            readDir(subpath, obj);
        } else if (stat.isFile()) {
            // Size in Bytes
            size = stat['size'];
            md5 = crypto
                .createHash('md5')
                .update(fs.readFileSync(subpath))
                .digest('hex');
            compressed = path.extname(subpath).toLowerCase() === '.zip';

            relative = path.relative(src, subpath);
            relative = relative.replace(/\\/g, '/');
            relative = encodeURI(relative);
            obj[relative] = {
                size: size,
                md5: md5,
            };
            if (compressed) {
                obj[relative].compressed = true;
            }
        }
    }
}

var mkdirSync = function (path) {
    try {
        fs.mkdirSync(path);
    } catch (e) {
        if (e.code != 'EEXIST') throw e;
    }
};

// Iterate assets and src folder
readDir(path.join(src, 'src'), manifest.assets);
readDir(path.join(src, 'assets'), manifest.assets);
readDir(path.join(src, 'jsb-adapter'), manifest.assets);

var destManifest = path.join(dest, 'project.manifest');
var destVersion = path.join(dest, 'version.manifest');

mkdirSync(dest);

fs.writeFile(destManifest, JSON.stringify(manifest), (err) => {
    if (err) throw err;
    console.log('Manifest successfully generated');
});

delete manifest.assets;
delete manifest.searchPaths;
fs.writeFile(destVersion, JSON.stringify(manifest), (err) => {
    if (err) throw err;
    console.log('Version successfully generated');
});





